# standard import
import os
import time
from threading import Timer
from socket import gaierror
import logging

# third party import
import board
import busio
from digitalio import Direction, Pull
from RPi import GPIO
from adafruit_mcp230xx.mcp23017 import MCP23017
from paho.mqtt import client as mqtt

# local import
from config import Config

if os.getenv('DEBUG', 1):
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
else:
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.WARNING)

logger = logging.getLogger('buttons-mqtt')

def handle_switch(port):
    """Callback function to be called when an Interrupt occurs."""
    address = Config.MCP_IRP[port]
    try:
        mcp = mcp_chips[address]
    except KeyError:
        return 0 # bad chip
    #print("Interrupt from device with address: {}".format(address))
    #print(mcp.int_flag)
    for pin_flag in mcp.int_flag:
        logger.debug(f'interrupt from device {address} and pin {port}')
        #print("Interrupt connected to Pin: {}".format(port))
        #print("Pin number: {} changed to: {}".format(pin_flag, mcp.get_pin(pin_flag).value))
        try:
            button = Config.MCP[address][pin_flag]
        except KeyError:
            return 0 # wrong pin or chip, faulty interrupt

        start = time.time()
        pressed = False
        while not mcp.get_pin(pin_flag).value:
            pressed = True
            if (time.time()-start)>1:
                logger.debug(f'hold {button}')
                mqttc.publish(f'/button/{button}', 'hold')
                time.sleep(0.2)

        if pressed:
            if (time.time()-start)<1:
                logger.debug(f'push {button}')
                mqttc.publish(f'/button/{button}', 'push')

    mcp.clear_ints()
    return 0


# Initialize the I2C bus:
i2c = busio.I2C(board.SCL, board.SDA)

# Initialize the MCP23017 chip on the bonnet
mcp_chips = {}
for address, value in Config.MCP.items():
    try:
        mcp_chips[address] = MCP23017(i2c, address=address)
        logger.debug(f'MCP23017 with address {address} initialized')
    except ValueError:
        pass

# Initialize the mqtt connection
mqttc = mqtt.Client('buttons')
mqttc.username_pw_set('mqtt_user', 'mqtt_pass')

connected = False
while not connected:
    try:
        mqttc.connect('addon_core_mosquitto')
        connected = True
        logger.debug('mqtt connected')
    except gaierror:
        pass

mqttc.loop_start()


if __name__ == "__main__":

    GPIO.cleanup()

    for address, mcp in mcp_chips.items():
        mcp.clear_ints()
        for idx in range(0,16):
            pin = mcp.get_pin(idx)
            pin.direction = Direction.INPUT
            pin.pull = Pull.UP
        mcp.interrupt_enable = 0xFFFF  # Enable Interrupts in all pins
        mcp.interrupt_configuration = 0xFFFF  # interrupt on any change
        mcp.default_value = 0xFFFF  # default value is 'high' so notify whenever 'low'
        mcp.io_control = 0x44  # Interrupt as open drain and mirrored
        mcp.clear_ints()  # Interrupts need to be cleared initially

        # Or, we can ask to be notified CONTINUOUSLY if a pin goes LOW (button press)
        # we won't get an IRQ pulse when the pin is HIGH!
        #mcp.interrupt_configuration = 0xFFFF         # notify pin value
        #mcp.default_value = 0xFFFF         # default value is 'high' so notify whenever 'low'

    # connect either interrupt pin to the Raspberry pi's pin 17.
    # They were previously configured as mirrored.
    GPIO.setmode(GPIO.BCM)

    for pin, address in Config.MCP_IRP.items():
        GPIO.setup(pin, GPIO.IN, GPIO.PUD_UP)  # Set up Pi's pin as input, pull up
        GPIO.add_event_detect(pin, GPIO.FALLING, callback=handle_switch, bouncetime=10)
        logger.debug(f'setup interrupt for MCP23017 with addres {address}')

    try:
        while True:
            time.sleep(1)

            for address, mcp in mcp_chips.items():
                mcp.clear_ints()

    except:
        GPIO.cleanup()
#        MCP.remove_interrupt(10)
    finally:
        GPIO.cleanup()
#        MCP.__del__()
