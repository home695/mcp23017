FROM python:3.10-alpine

COPY ./src/ /app/
COPY ./requirements.txt /app/
WORKDIR /app/

RUN apk update && apk add python3-dev \
                          gcc \
                          libc-dev \
                          libffi-dev

# this has to be done to make rpi.gpio install work
ENV CFLAGS=-fcommon

EXPOSE 1883

RUN pip install -r requirements.txt

CMD ["python", "main.py"]
