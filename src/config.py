class Config():

    MCP = {
        0x20: {
            0: 'halup',
            1: 'washr',
            2: 'bathr',
            3: 'sl3',
            4: 'sl2',
            5: 'sl1',
            6: 'attS',
            7: 'attN',
            8: '',
            9: '',
            10: '',
            11: 'haldown',
        },
        0x24: {
            0: 'office',
            1: 'toilet',
            2: 'haldown',
            3: 'living',
            4: 'dinner',
            5: 'kitch',
            6: 'garden1',
            7: 'garden2',
            8: '',
            9: '',
            10: '',
            11: 'halup',
        }
    }

    MCP_IRP = {
        17: 0x20,
        27: 0x24,
    }
